<html>
    <head>
        <title>Brevet Times</title>
    </head>

    <body>
        <h1>Brevet Times</h1>

        <h2>List All Times - JSON</h2>
        <ul>
            <?php
            $json = file_get_contents('http://web/listAll/json/');
            $obj = json_decode($json);
	        $open_times = $obj->open;
            foreach ($open_times as $l) {
                echo "<li>$l</li>";
            }
            $close_times = $obj->close;
            foreach ($close_times as $l) {
                echo "<li>$l</li>";
            }
            ?>
        </ul>

        <h2>List Open Times - JSON</h2>
        <ul>
            <?php
            $json = file_get_contents('http://web/listOpenOnly/json');
            $obj = json_decode($json);
            $open_times = $obj->open;
            foreach ($open_times as $l) {
                echo "<li>$l</li>";
            }
            ?>
        </ul>

        <h2>List Close Times - JSON</h2>
        <ul>
            <?php
            $json = file_get_contents('http://web/listCloseOnly/json');
            $obj = json_decode($json);
            $open_times = $obj->close;
            foreach ($open_times as $l) {
                echo "<li>$l</li>";
            }
            ?>
        </ul>

        <h2>List All Times - CSV</h2>
        <ul>
            <?php
            $csv = file_get_contents('http://web/listAll/csv/');
            $obj = json_decode($csv);
            foreach ($obj as $l) {
                echo "<li>$l</li>";
            }
            ?>
        </ul>

        <h2>List Open Times - CSV</h2>
        <ul>
            <?php
            $csv = file_get_contents('http://web/listOpenOnly/csv/');
            $obj = json_decode($csv);
            foreach ($obj as $l) {
                echo "<li>$l</li>";
            }
            ?>
        </ul>

        <h2>List Close Times - CSV</h2>
        <ul>
            <?php
            $csv = file_get_contents('http://web/listCloseOnly/csv/');
            $obj = json_decode($csv);
            foreach ($obj as $l) {
                echo "<li>$l</li>";
            }
            ?>
        </ul>

        <h2>List Open Times - JSON - Top 2</h2>
        <ul>
            <?php
            $json = file_get_contents('http://web/listOpenOnly/json/?top=2');
            $obj = json_decode($json);
            $open_times = $obj->open;
            foreach ($open_times as $l) {
                echo "<li>$l</li>";
            }
            ?>
        </ul>

        <h2>List Close Times - CSV - Top 2</h2>
        <ul>
            <?php
            $csv = file_get_contents('http://web/listCloseOnly/csv/?top=2');
            $obj = json_decode($csv);
            foreach ($obj as $l) {
                echo "<li>$l</li>";
            }
            ?>
        </ul>

    </body>
</html>
